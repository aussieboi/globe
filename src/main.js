import Vue from 'vue';
import App from './App.vue';
import * as VueGoogleMaps from 'vue2-google-maps';
import axios from 'axios';

Vue.prototype.axios = axios;

Vue.config.devtools = true;
Vue.config.productionTip = false;

Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyAHzGuClyQ71dSFjC_cXJ-5dbb9xE6MFRg',
    libraries: 'places' // This is required if you use the Autocomplete plugin
    // OR: libraries: 'places,drawing'
    // OR: libraries: 'places,drawing,visualization'
    // (as you require)
  }
});

new Vue({
  render: h => h(App)
}).$mount('#app');
